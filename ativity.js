
// insert one room
db.users.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});


// insert mulitple rooms
db.users.insertMany([
		{
		name: "double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{

		name: "queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
		
]);

// search for a room with the name double
db.users.find({name: "double"});

// update the queen room and set the available rooms to 0

db.users.updateOne(
	{name:"queen"}, {
		$set:{
			rooms_available: 0,
		}});

// delete all rooms that have 0 availability
db.users.deleteMany({
	rooms_available: 0,
});


// final db outcome
/* 1 */
{
    "_id" : ObjectId("6352536f398413d799d2ae88"),
    "name" : "single",
    "accommodates" : 2.0,
    "description" : "A simple room with all the basic necessities",
    "rooms_available" : 10.0,
    "isAvailable" : false,
    "price" : 1000.0
}

/* 2 */
{
    "_id" : ObjectId("6352549b398413d799d2ae89"),
    "name" : "double",
    "accommodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_available" : 5.0,
    "isAvailable" : false
}